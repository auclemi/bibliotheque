import { Component } from '@angular/core';
import * as firebase from 'firebase';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bibliothèque';
  constructor() {
    var config = {
      apiKey: "AIzaSyBcsXZ_KmzBdddGiSgAyKjFp6x4-RDjcpM",
      authDomain: "bibliotheque-6e0e2.firebaseapp.com",
      databaseURL: "https://bibliotheque-6e0e2.firebaseio.com",
      projectId: "bibliotheque-6e0e2",
      storageBucket: "bibliotheque-6e0e2.appspot.com",
      messagingSenderId: "940614046760"
    };
    firebase.initializeApp(config);
  }
}
